﻿using Indigo.TestClient.Core;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Indigo.TestClient.ViewModels
{
    public class TestClientViewModel : BindableBase
    {
        private Client client;
        private string serverURL = "http://localhost:8081/signalr";
        private string name;
        private string message;
        private string board;

        public DelegateCommand LogInCommand { get; private set; }
        public DelegateCommand SendMessageCommand { get; private set; }

        public TestClientViewModel()
        {
            LogInCommand = new DelegateCommand(OnLogIn, CanLogIn);
            SendMessageCommand = new DelegateCommand(OnSendMessage, CanSendMessage);
            this.client = new Client(this, ServerURL);
        }

        public string ServerURL
        {
            get { return this.serverURL; }
            set { SetProperty(ref this.serverURL, value); }

        }

        public string Name
        {
            get { return this.name; }
            set { SetProperty(ref this.name, value); }
        }

        public string Message
        {
            get { return this.message; }
            set { SetProperty(ref this.message, value); }
        }

        public string Board
        {
            get { return this.board; }
            set { SetProperty(ref this.board, value + Environment.NewLine); }
        }

        #region Command methods

        private void OnLogIn()
        {
            this.client.Connect();
            LogInCommand.RaiseCanExecuteChanged();
            SendMessageCommand.RaiseCanExecuteChanged();
        }

        private bool CanLogIn()
        {
            return !this.client.IsConnected;
        }

        private void OnSendMessage()
        {
            this.client.Broadcast(Name, Message);
        }

        private bool CanSendMessage()
        {
            return this.client.IsConnected;
        }

        #endregion // Command methods
    }
}
