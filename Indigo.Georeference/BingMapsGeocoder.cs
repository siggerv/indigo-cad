﻿using Indigo.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace Indigo.Georeference
{
    public class BingMapsGeocoder
    {
        public Response MakeRequest(string requestUrl)
        {
            HttpWebRequest request = WebRequest.Create(requestUrl) as HttpWebRequest;

            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new Exception(String.Format("Server error (HTTP {0}: {1}).", response.StatusCode, response.StatusDescription));
                }

                DataContractJsonSerializer jsonSerializaer = new DataContractJsonSerializer(typeof(Response));
                object objResponse = jsonSerializaer.ReadObject(response.GetResponseStream());
                Response jsonResponse = objResponse as Response;

                return jsonResponse;
            }
        }
    }
}
