﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Indigo.Services.Contracts
{
    [DataContract]
    public class BoundingBox
    {
        [DataMember(Name = "southLatitude")]
        public double SouthLatitude { get; set; }
        [DataMember(Name = "westLongitude")]
        public double WestLongitude { get; set; }
        [DataMember(Name = "northLatitude")]
        public double NorthLatitude { get; set; }
        [DataMember(Name = "eastLongitude")]
        public double EastLongitude { get; set; }
    }
}
