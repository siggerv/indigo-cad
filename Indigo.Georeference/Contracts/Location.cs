﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Indigo.Services.Contracts
{
    [DataContract(Namespace = "http://schemas.microsoft.com/search/local/ws/rest/v1")]
    public class Location
    {
        [DataMember(Name = "boundingBox")]
        public BoundingBox BoundingBox { get; set; }
        [DataMember(Name = "name")]
        public string Name { get; set; }
        [DataMember(Name = "point")]
        public Point Point { get; set; }
        [DataMember(Name = "entityType")]
        public string EntityType { get; set; }
        [DataMember(Name = "address")]
        public Address Address { get; set; }
        [DataMember(Name = "confidence")]
        public string Confidence { get; set; }
        [DataMember(Name = "geocodePoints")]
        public GeocodePoint[] GeocodePoints { get; set; }
        [DataMember(Name = "matchCodes")]
        public string[] MatchCodes { get; set; }
    }
}
