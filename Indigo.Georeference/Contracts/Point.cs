﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Indigo.Services.Contracts
{
    [DataContract]
    public class Point
    {
        /// <summary>
        /// Latitude,Longitude
        /// </summary>
        [DataMember(Name = "coordinates")]
        public double[] Coordinates { get; set; }
    }
}
