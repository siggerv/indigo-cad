﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Indigo.Services.Contracts
{
    [DataContract]
    public class GeocodePoint : Point
    {
        [DataMember(Name = "calculationMethod")]
        public string CalculationMethod { get; set; }
        [DataMember(Name = "usageTypes")]
        public string[] UsageTypes { get; set; }
    }
}
