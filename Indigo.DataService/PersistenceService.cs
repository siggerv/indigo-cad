﻿using Indigo.Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Indigo.DataService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "PersistenceService" in both code and config file together.
    public class PersistenceService : IPersistenceService
    {
        public void AddEvent(Event evt)
        {
            using (var context = new IndigoModelContainer())
            {
                context.Entry(evt).State = EntityState.Added;
                context.SaveChanges();
            }
        }

        public IEnumerable<Event> GetEvents()
        {
            using (var context = new IndigoModelContainer())
            {
                var result = context.EventSet.ToList();
                result.ForEach(e => context.Entry(e).State = EntityState.Detached);
                return result;
            }
        }
    }
}
