﻿using Indigo.WebServer.Core;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Indigo.WebServer.ViewModels
{
    public class WebServerViewModel : BindableBase
    {
        private Server server;
        private string serverURI = "http://localhost:8081";
        private string log;

        public DelegateCommand StartServerCommand { get; private set; }
        public DelegateCommand StopServerCommand { get; private set; }

        public WebServerViewModel()
        {
            StartServerCommand = new DelegateCommand(OnStartServer, CanStartServer);
            StopServerCommand = new DelegateCommand(OnStopServer, CanStopServer);
            this.server = new Server(this.serverURI);
        }

        public string ServerURI
        {
            get { return this.serverURI; }
            set { SetProperty(ref this.serverURI, value); }
        }

        public string Log
        {
            get { return this.log; }
            set { SetProperty(ref this.log, value + Environment.NewLine); }
        }

        #region Command methods

        private void OnStartServer()
        {
            this.server.Start();
            StartServerCommand.RaiseCanExecuteChanged();
            StopServerCommand.RaiseCanExecuteChanged();
            Log += "Server Started on " + ServerURI;
        }

        private bool CanStartServer()
        {
            return !this.server.IsConnected;
        }

        private void OnStopServer()
        {
            this.server.Stop();
            StopServerCommand.RaiseCanExecuteChanged();
            StartServerCommand.RaiseCanExecuteChanged();
            Log += "Server Stopped on " + ServerURI;
        }

        private bool CanStopServer()
        {
            return this.server.IsConnected;
        }

        #endregion // Command methods
    }
}
