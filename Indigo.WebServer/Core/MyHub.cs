﻿using Indigo.Messaging;
using Indigo.WebServer.ViewModels;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Indigo.WebServer.Core
{
    public class MyHub : Hub<IClient>, IServer //Use Application.Current.Dispatcher to access UI thread from outside the MainWindow class
    {
        private WebServerViewModel viewModel;

        public MyHub()
        {
            Application.Current.Dispatcher.Invoke(() =>
                {
                    this.viewModel = (WebServerViewModel)Application.Current.MainWindow.DataContext;
                });
        }

        public override Task OnConnected()
        {
            Application.Current.Dispatcher.Invoke(() =>
                {
                    this.viewModel.Log += ("Client connected: " + Context.ConnectionId);
                });

            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            Application.Current.Dispatcher.Invoke(() =>
                {
                    this.viewModel.Log += ("Client disconnected: " + Context.ConnectionId);
                });

            return base.OnDisconnected(stopCalled);
        }

        #region Server methods

        public void Broadcast(string name, string message)
        {
            Clients.All.AddMessage(name, message);
        }

        #endregion // Server methods
    }
}
