﻿using Microsoft.AspNet.SignalR;
using Microsoft.Owin.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Indigo.WebServer.Core
{
    public class Server
    {
        private readonly static IHubContext hubContext = GlobalHost.ConnectionManager.GetHubContext<MyHub>();
        private readonly string serverURI;
        public IDisposable SignalR { get; private set; }
        public bool IsConnected { get; private set; }

        public Server(string uri)
        {
            this.serverURI = uri;
            IsConnected = false;
        }

        public void Start()
        {
            SignalR = WebApp.Start(serverURI);
            IsConnected = true;
        }

        public void Stop()
        {
            SignalR.Dispose();
            IsConnected = false;
        }
    }
}
